﻿using Company.Domain.Enum;
using Company.Domain.Models;
using Company.Domain.Models.Response;
using Company.Repository.Entities;
using Company.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company.Repository
{
    public class LeadRepository : ILeadRepository
    {
        protected readonly CompanyContext _context;

        public LeadRepository(CompanyContext context)
        {
            _context = context;
        }

        public ResponseModel AddLead(AddLeadModel model)
        {
            var date = DateTime.Now;
            var Lead = new Lead
            {
                FullName = model.FullName,
                Email = model.Email,
                Mobile = model.Mobile,
                Comments = model.Comments,
                Date = date,
            };

            _context.Add(Lead);
            int count = _context.SaveChanges();

            return new ResponseModel { Success = count > 0 };
        }

        public PagedResponseModel GetLeads(int limit, int offset)
        {
            var response = new PagedResponseModel { Success = true };
            response.Total = _context.Lead.Count();
            if (response.Total == 0)
            {
                return response;
            }

            var leads = _context.Lead.OrderByDescending(x => x.LeadId).Skip(offset).Take(limit).Select(x => new LeadModel
            {
                LeadId = x.LeadId,
                FullName = x.FullName,
                Email = x.Email,
                Mobile = x.Mobile,
                Comments = x.Comments,
                Date = x.Date,
            }).ToList();

            response.Response = leads;
            return response;
        }
    }
}
