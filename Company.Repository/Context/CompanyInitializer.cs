﻿using Company.Domain;
using Company.Repository.Entities;
using Company.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

using Microsoft.AspNetCore.Identity;

namespace Company.Repository
{
    public class CompanyInitializer
    {
        protected CompanyInitializer() { }

        public static void Initialize(CompanyContext context, UserManager<ApplicationUser> userManager,
       RoleManager<ApplicationRole> roleManager, ILogger<CompanyInitializer> logger)
        {
            // Look for any users.
            if (context.Users.Any())
            {
                return; // DB has been seeded
            }

            CreateDefaultUserAndRoleForApplication(userManager, roleManager, logger);
        }

        private static void CreateDefaultUserAndRoleForApplication(UserManager<ApplicationUser> um, RoleManager<ApplicationRole> rm, ILogger<CompanyInitializer> logger)
        {
            const string administratorRole = "SuperAdmin";
            const string phone = "1111111111";

            CreateDefaultAdministratorRole(rm, logger, administratorRole);
            var user = CreateDefaultUser(um, logger, phone);
            SetPasswordForDefaultUser(um, logger, phone, user);
            AddDefaultRoleToDefaultUser(um, logger, phone, administratorRole, user);

            CreateDefaultAdministratorRole(rm, logger, "Admin");
            CreateDefaultAdministratorRole(rm, logger, "Customer");
            CreateDefaultAdministratorRole(rm, logger, "Employee");
        }

        private static void CreateDefaultAdministratorRole(RoleManager<ApplicationRole> rm, ILogger<CompanyInitializer> logger, string administratorRole)
        {
            logger.LogInformation($"Create the role `{administratorRole}` for application");
            var ir = rm.CreateAsync(new ApplicationRole { Name = administratorRole });
            if (ir.Result.Succeeded)
            {
                logger.LogDebug($"Created the role `{administratorRole}` successfully");
            }
            else
            {
                var exception = new Exception($"Default role `{administratorRole}` cannot be created");
                logger.LogError(exception, GetIdentiryErrorsInCommaSeperatedList(ir.Result));
                throw exception;
            }
        }

        private static ApplicationUser CreateDefaultUser(UserManager<ApplicationUser> um, ILogger<CompanyInitializer> logger, string phone)
        {
            logger.LogInformation($"Create default user with mobile `{phone}` for application");
            var user = new ApplicationUser
            {
                UserName = phone,
                FullName = "Super",
                Email = "noreply@awp.com",
                PhoneNumber = phone,
                PhoneNumberConfirmed = true,
                UserInfotmation = new UserInfotmation
                {
                    FullName = "Super",
                    Mobile = phone,
                    Email = "noreply@awp.com",
                    CreateDate = DateTime.Now
                }
            };

            var ir = um.CreateAsync(user);
            if (ir.Result.Succeeded)
            {
                logger.LogDebug($"Created default user `{phone}` successfully");
            }
            else
            {
                var exception = new Exception($"Default user `{phone}` cannot be created");
                logger.LogError(exception, GetIdentiryErrorsInCommaSeperatedList(ir.Result));
                throw exception;
            }

            var createdUser = um.FindByNameAsync(phone);
            return createdUser.Result;
        }

        private static void SetPasswordForDefaultUser(UserManager<ApplicationUser> um, ILogger<CompanyInitializer> logger, string phone, ApplicationUser user)
        {
            logger.LogInformation($"Set password for default user `{phone}`");
            const string password = "123456";
            var ir = um.AddPasswordAsync(user, password);
            if (ir.Result.Succeeded)
            {
                logger.LogTrace($"Set password `{password}` for default user `{phone}` successfully");
            }
            else
            {
                var exception = new Exception($"Password for the user `{phone}` cannot be set");
                logger.LogError(exception, GetIdentiryErrorsInCommaSeperatedList(ir.Result));
                throw exception;
            }
        }

        private static void AddDefaultRoleToDefaultUser(UserManager<ApplicationUser> um, ILogger<CompanyInitializer> logger, string phone, string administratorRole, ApplicationUser user)
        {
            logger.LogInformation($"Add default user `{phone}` to role '{administratorRole}'");
            var ir = um.AddToRoleAsync(user, administratorRole);
            if (ir.Result.Succeeded)
            {
                logger.LogDebug($"Added the role '{administratorRole}' to default user `{phone}` successfully");
            }
            else
            {
                var exception = new Exception($"The role `{administratorRole}` cannot be set for the user `{phone}`");
                logger.LogError(exception, GetIdentiryErrorsInCommaSeperatedList(ir.Result));
                throw exception;
            }
        }

        private static string GetIdentiryErrorsInCommaSeperatedList(IdentityResult ir)
        {
            string errors = null;
            foreach (var identityError in ir.Errors)
            {
                errors += identityError.Description;
                errors += ", ";
            }
            return errors;
        }
    }
}
