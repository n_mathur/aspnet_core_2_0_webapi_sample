﻿using Company.Repository.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace Company.Repository
{
    public class CompanyContext : IdentityDbContext<ApplicationUser, ApplicationRole, int>
    {
        public CompanyContext(DbContextOptions<CompanyContext> options)
            : base(options)
        {
        }

        public DbSet<UserInfotmation> UserInfotmation { get; set; }

        public DbSet<Department> Department { get; set; }

        public DbSet<Lead> Lead { get; set; }
        public DbSet<Event> Event { get; set; }

        public DbSet<Media> Media { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<ProductMedia> ProductMedia { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }

    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<CompanyContext>
    {
        public CompanyContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            var builder = new DbContextOptionsBuilder<CompanyContext>();
            var connectionString = configuration.GetConnectionString("CompanyConnection");
            builder.UseMySql(connectionString);
            return new CompanyContext(builder.Options);
        }
    }
}
