﻿using Company.Repository;
using Company.Domain.Models;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Company.Repository.Entities
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser<int>
    {
        public string FullName { get; set; }
        public virtual UserInfotmation UserInfotmation { get; set; }
    }

    public class ApplicationRole : IdentityRole<int>
    {
    }
}
