﻿using Company.Domain.Models;
using Company.Repository.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Company.Repository.Entities
{
    public class Department : Base
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DepartmentId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
