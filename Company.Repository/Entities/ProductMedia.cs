﻿using Company.Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Company.Repository.Entities
{
    public class ProductMedia
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProductMediaId { get; set; }        
        public int ProductId { get; set; }
        public int MediaId { get; set; }
    }
}
