﻿using Company.Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Company.Repository.Entities
{
    public class Media : Base
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MediaId { get; set; }
        public string Title { get; set; }
        public string Filename { get; set; }
        public string Description { get; set; }
    }
}
