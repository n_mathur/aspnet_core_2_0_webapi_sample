﻿using Company.Domain.Models;
using Company.Repository.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Company.Repository.Entities
{
    public class UserInfotmation
    {
        [Key, ForeignKey("User")]
        public int UserId { get; set; }
        public string FullName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public DateTime CreateDate { get; set; }
        public int MediaId { get; set; }
        public string Role { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
