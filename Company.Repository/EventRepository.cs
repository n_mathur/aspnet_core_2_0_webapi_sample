﻿using Company.Domain.Enum;
using Company.Domain.Models;
using Company.Domain.Models.Response;
using Company.Repository.Entities;
using Company.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company.Repository
{
    public class EventRepository : IEventRepository
    {
        protected readonly CompanyContext _context;

        public EventRepository(CompanyContext context)
        {
            _context = context;
        }

        public ResponseModel AddEvent(AddEventModel model, int createBy)
        {
            var date = DateTime.Now;
            var _event = new Event
            {
                Title = model.Title,
                Description = model.Description,
                StartDate = model.Start,
                EndDate = model.End,
                Active = true,
                CreateBy = createBy,
                CreateDate = date,
                ModifyBy = createBy,
                ModifyDate = date
            };

            _context.Add(_event);
            int count = _context.SaveChanges();

            return new ResponseModel { Success = count > 0 };
        }

        public ResponseModel UpdateEvent(UpdateEventModel model, int modifyBy)
        {
            var _event = _context.Event.Where(x => x.EventId == model.EventId).FirstOrDefault();
            if (_event == null)
            {
                return new ResponseModel { Success = true };
            }

            var date = DateTime.Now;

            _event.Title = model.Title;
            _event.Description = model.Description;
            _event.StartDate = model.Start;
            _event.EndDate = model.End;
            _event.Active = model.Active;
            _event.ModifyBy = modifyBy;
            _event.ModifyDate = date;

            _context.Update(_event);
            int count = _context.SaveChanges();

            return new ResponseModel { Success = count > 0 };
        }

        public ResponseModel DeleteEvent(int eventId)
        {
            var Event = _context.Event.Where(x => x.EventId == eventId).FirstOrDefault();

            if (Event == null)
            {
                return new ResponseModel { Success = true };
            }

            _context.Remove(Event);
            int count = _context.SaveChanges();

            return new ResponseModel { Success = count > 0 };
        }

        public EventModel GetEvent(int eventId)
        {
            var _event = _context.Event.Where(x => x.EventId == eventId).FirstOrDefault();
            if (_event == null)
            {
                return new EventModel();
            }
            return new EventModel
            {
                EventId = _event.EventId,
                Title = _event.Title,
                Description = _event.Description,
                StartDate = _event.StartDate,
                EndDate = _event.EndDate,
                Active = _event.Active,
                CreateBy = _event.CreateBy,
                CreateDate = _event.CreateDate,
                ModifyBy = _event.ModifyBy,
                ModifyDate = _event.ModifyDate
            };
        }

        public PagedResponseModel GetEvents(int limit, int offset)
        {
            var response = new PagedResponseModel { Success = true };
            response.Total = _context.Event.Count();
            if (response.Total == 0)
            {
                return response;
            }
            var events = _context.Event.OrderByDescending(x => x.CreateDate).Skip(offset).Take(limit).Select(x => new EventModel
            {
                EventId = x.EventId,
                Title = x.Title,
                Description = x.Description,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                Active = x.Active,
                CreateBy = x.CreateBy,
                CreateDate = x.CreateDate,
                ModifyBy = x.ModifyBy,
                ModifyDate = x.ModifyDate
            }).ToList();

            response.Response = events;
            return response;
        }        
    }
}
