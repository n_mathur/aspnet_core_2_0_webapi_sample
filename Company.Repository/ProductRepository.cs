﻿using Company.Domain.Enum;
using Company.Domain.Models;
using Company.Domain.Models.Response;
using Company.Repository.Entities;
using Company.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company.Repository
{
    public class ProductRepository : IProductRepository
    {
        protected readonly CompanyContext _context;

        public ProductRepository(CompanyContext context)
        {
            _context = context;
        }

        public ResponseModel AddProducts(List<AddBulkProductModel> models, int createBy)
        {
            var date = DateTime.Now;
            foreach (var model in models)
            {
                var product = new Product
                {
                    Title = model.Title,
                    Description = model.Description,
                    Quantity = model.Quantity,
                    Discount = model.Discount,
                    Price = model.Price,
                    Active = true,
                    CreateBy = createBy,
                    CreateDate = date,
                    ModifyBy = createBy,
                    ModifyDate = date
                };

                _context.Add(product);
            }

            int count = _context.SaveChanges();
            return new ResponseModel { Success = count > 0 };
        }

        public ResponseModel AddProduct(AddProductModel model, int createBy)
        {
            var date = DateTime.Now;
            var product = new Product
            {
                Title = model.Title,
                Description = model.Description,
                Quantity = model.Quantity,
                Discount = model.Discount,
                Price = model.Price,
                Active = true,
                CreateBy = createBy,
                CreateDate = date,
                ModifyBy = createBy,
                ModifyDate = date
            };

            _context.Add(product);
            int count = _context.SaveChanges();

            if (count > 0)
            {
                if (!string.IsNullOrEmpty(model.MediaIds))
                {
                    var mediaIds = model.MediaIds.Split(",").Select(Int32.Parse).ToList();
                    AddProductMedia(product.ProductId, mediaIds);
                }
                return new ResponseModel { Success = true };
            }
            return new ResponseModel { Success = false };
        }

        public ResponseModel UpdateProduct(UpdateProductModel model, int modifyBy)
        {
            var product = _context.Product.Where(x => x.ProductId == model.ProductId).FirstOrDefault();
            if (product == null)
            {
                return new ResponseModel { Success = true };
            }

            var date = DateTime.Now;

            product.Title = model.Title;
            product.Description = model.Description;
            product.Quantity = model.Quantity;
            product.Discount = model.Discount;
            product.Price = model.Price;
            product.Active = model.Active;
            product.ModifyBy = modifyBy;
            product.ModifyDate = date;

            _context.Update(product);
            int count = _context.SaveChanges();

            if (count > 0)
            {
                if (!string.IsNullOrEmpty(model.MediaIds))
                {
                    var mediaIds = model.MediaIds.Split(",").Select(Int32.Parse).ToList();
                    var existingMedias = _context.ProductMedia.Where(x => x.ProductId == model.ProductId).Select(x => x.MediaId).ToList();
                    if (existingMedias != null)
                    {
                        var newMediaIds = mediaIds.Where(x => !existingMedias.Contains(x)).ToList();
                        AddProductMedia(product.ProductId, newMediaIds);
                    }
                }
                return new ResponseModel { Success = true };
            }
            return new ResponseModel { Success = false };
        }

        public ResponseModel DeleteProduct(int productId)
        {
            var Product = _context.Product.Where(x => x.ProductId == productId).FirstOrDefault();

            if (Product == null)
            {
                return new ResponseModel { Success = true };
            }

            _context.Remove(Product);
            int count = _context.SaveChanges();

            return new ResponseModel { Success = count > 0 };
        }

        public ProductModel GetProduct(int productId)
        {
            var _product = _context.Product.Where(x => x.ProductId == productId).FirstOrDefault();
            if (_product == null)
            {
                return new ProductModel();
            }
            return new ProductModel
            {
                ProductId = _product.ProductId,
                Title = _product.Title,
                Description = _product.Description,
                Quantity = _product.Quantity,
                Discount = _product.Discount,
                Price = _product.Price,
                CreateBy = _product.CreateBy,
                CreateDate = _product.CreateDate,
                ModifyBy = _product.ModifyBy,
                ModifyDate = _product.ModifyDate
            };
        }

        public List<ProductModel> GetProducts()
        {
            var products = _context.Product.Select(x => new ProductModel
            {
                ProductId = x.ProductId,
                Title = x.Title,
                Description = x.Description,
                Quantity = x.Quantity,
                Discount = x.Discount,
                Price = x.Price,
                CreateBy = x.CreateBy,
                CreateDate = x.CreateDate,
                ModifyBy = x.ModifyBy,
                ModifyDate = x.ModifyDate
            }).ToList();

            return products;
        }

        public ResponseModel AddProductMedia(int productId, List<int> mediaIds)
        {
            foreach (var mediaId in mediaIds)
            {
                var Product = new ProductMedia
                {
                    MediaId = mediaId,
                    ProductId = productId
                };

                _context.Add(Product);
            }
            int count = _context.SaveChanges();

            return new ResponseModel { Success = count > 0 };
        }

        public List<ProductMediaModel> GetProductMedia(int productId)
        {
            var productMedias = _context.ProductMedia.Where(x => x.ProductId == productId).ToList();
            return productMedias.Select(x => new ProductMediaModel
            {
                ProductMediaId = x.ProductMediaId,
                ProductId = x.ProductId,
                MediaId = x.MediaId
            }).ToList();
        }

        public List<ProductMediaModel> GetProductMedia(List<int> productIds)
        {
            var productMedias = _context.ProductMedia.Where(x => productIds.Contains(x.ProductId)).ToList();
            return productMedias.Select(x => new ProductMediaModel
            {
                ProductMediaId = x.ProductMediaId,
                ProductId = x.ProductId,
                MediaId = x.MediaId
            }).ToList();
        }

        public ResponseModel DeleteProductMedia(int productMediaId)
        {
            var productMedia = _context.ProductMedia.Where(x => x.ProductMediaId == productMediaId).FirstOrDefault();

            if (productMedia == null)
            {
                return new ResponseModel { Success = true };
            }

            _context.Remove(productMedia);
            int count = _context.SaveChanges();

            return new ResponseModel { Success = count > 0 };
        }
    }
}
