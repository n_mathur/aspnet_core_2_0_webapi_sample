﻿using Company.Domain.Enum;
using Company.Domain.Models;
using Company.Domain.Models.Response;
using Company.Repository.Entities;
using Company.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company.Repository
{
    public class DepartmentRepository : IDepartmentRepository
    {
        protected readonly CompanyContext _context;

        public DepartmentRepository(CompanyContext context)
        {
            _context = context;
        }

        public ResponseModel AddDepartment(AddDepartmentModel model, int createBy)
        {
            var date = DateTime.Now;
            var department = new Department
            {
                Title = model.Title,
                Description = model.Description,              
                CreateBy = createBy,
                CreateDate = date,
                ModifyBy = createBy,
                ModifyDate = date
            };

            _context.Add(department);
            int count = _context.SaveChanges();
            return new ResponseModel { Success = count > 0 };
        }

        public ResponseModel UpdateDepartment(UpdateDepartmentModel model, int modifyBy)
        {
            var department = _context.Department.Where(x => x.DepartmentId == model.DepartmentId).FirstOrDefault();
            if (department == null)
            {
                return new ResponseModel { Success = true };
            }

            var date = DateTime.Now;

            department.Title = model.Title;
            department.Description = model.Description;           
            department.ModifyBy = modifyBy;
            department.ModifyDate = date;

            _context.Update(department);
            int count = _context.SaveChanges();            
            return new ResponseModel { Success = count > 0 };
        }

        public ResponseModel DeleteDepartment(int departmentId)
        {
            var Department = _context.Department.Where(x => x.DepartmentId == departmentId).FirstOrDefault();

            if (Department == null)
            {
                return new ResponseModel { Success = true };
            }

            _context.Remove(Department);
            int count = _context.SaveChanges();

            return new ResponseModel { Success = count > 0 };
        }

        public DepartmentModel GetDepartment(int departmentId)
        {
            var _department = _context.Department.Where(x => x.DepartmentId == departmentId).FirstOrDefault();
            if (_department == null)
            {
                return new DepartmentModel();
            }
            return new DepartmentModel
            {
                DepartmentId = _department.DepartmentId,
                Title = _department.Title,
                Description = _department.Description,
                CreateBy = _department.CreateBy,
                CreateDate = _department.CreateDate,
                ModifyBy = _department.ModifyBy,
                ModifyDate = _department.ModifyDate
            };
        }

        public List<DepartmentModel> GetDepartments()
        {
            var departments = _context.Department.Select(x => new DepartmentModel
            {
                DepartmentId = x.DepartmentId,
                Title = x.Title,
                Description = x.Description,
                CreateBy = x.CreateBy,
                CreateDate = x.CreateDate,
                ModifyBy = x.ModifyBy,
                ModifyDate = x.ModifyDate
            }).ToList();

            return departments;
        }        
    }
}
