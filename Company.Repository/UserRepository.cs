﻿using Company.Domain.Enum;
using Company.Domain.Models;
using Company.Domain.Models.Response;
using Company.Repository.Entities;
using Company.Repository.Interface;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly UserManager<ApplicationUser> _userManager;
        protected readonly CompanyContext _context;

        public UserRepository(UserManager<ApplicationUser> userManager, CompanyContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        public async Task<ResponseModel> RegisterUser(RegisterModel model)
        {
            var result = new ResponseModel();
            var role = Role.Employee.ToString(); //sample role employee
            var user = new ApplicationUser
            {
                FullName = model.FullName,
                UserName = model.Mobile,
                Email = model.Email,
                UserInfotmation = new UserInfotmation
                {
                    FullName = model.FullName,
                    Mobile = model.Mobile,
                    Email = model.Email,
                    Address = model.Address,
                    Role = role,
                    CreateDate = DateTime.Now
                }
            };
            var identityResult = await _userManager.CreateAsync(user, model.Password);
            result.Success = identityResult.Succeeded;
            if (identityResult.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, role);
                var token = await _userManager.GenerateChangePhoneNumberTokenAsync(user, model.Mobile);
                result.Response = token;
            }

            return result;
        }

        public async Task<ResponseModel> ConfirmMobile(MobileVerifyModel model)
        {
            var result = new ResponseModel();
            var user = await _userManager.FindByNameAsync(model.UserName);
            var tokenResult = await _userManager.ChangePhoneNumberAsync(user, model.UserName, model.Token);
            if (tokenResult.Succeeded)
            {
                result.Success = true;
            }
            return result;
        }

        public async Task<ResponseModel> Validate(LoginModel model)
        {
            var user = await _userManager.FindByNameAsync(model.Username);
            var valid = await _userManager.CheckPasswordAsync(user, model.Password);

            var result = new ResponseModel
            {
                Success = valid && user.PhoneNumberConfirmed
            };

            if (result.Success)
            {
                var userRoles = await _userManager.GetRolesAsync(user);
                result.Response = userRoles.FirstOrDefault(); //one user on role
            }

            return result;
        }

        public ProfileModel GetProfile(string userName)
        {
            var user = _context.UserInfotmation.Where(x => x.Mobile == userName).FirstOrDefault();
            var profile = new ProfileModel();
            if (user != null)
            {
                profile.UserId = user.UserId;
                profile.FullName = user.FullName;
                profile.Mobile = user.Mobile;
                profile.Address = user.Address;
                profile.Email = user.Email;
                profile.MediaId = user.MediaId;
                profile.Role = user.Role;
            }

            return profile;
        }

        public ProfileModel GetProfileByUser(int userId)
        {
            var user = _context.UserInfotmation.Where(x => x.UserId == userId).FirstOrDefault();
            var profile = new ProfileModel();
            if (user != null)
            {
                profile.UserId = user.UserId;
                profile.FullName = user.FullName;
                profile.Mobile = user.Mobile;
                profile.Address = user.Address;
                profile.Email = user.Email;
                profile.MediaId = user.MediaId;
                profile.Role = user.Role;
            }

            return profile;
        }

        public bool UpdateProfile(ProfileUpdateModel model, string userName)
        {
            var user = _context.UserInfotmation.Where(x => x.Mobile == userName).FirstOrDefault();

            user.FullName = model.FullName;
            user.Address = model.Address;
            user.Email = model.Email;
            user.MediaId = model.MediaId;

            var count = _context.SaveChanges();
            return count > 0;
        }

        public bool UpdateUserProfile(UserProfileUpdateModel model)
        {
            var user = _context.UserInfotmation.Where(x => x.UserId == model.UserId).FirstOrDefault();

            user.FullName = model.FullName;
            user.Address = model.Address;
            user.Email = model.Email;
            user.MediaId = model.MediaId;

            var count = _context.SaveChanges();
            return count > 0;
        }

        public async Task<bool> ChangePsssword(ChangePasswordModel model, string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            var result = await _userManager.ChangePasswordAsync(user, model.CurrentPassword, model.NewPassword);
            if (result.Succeeded)
            {
                return true;
            }
            return false;
        }

        public async Task<ResponseModel> ForgotPassword(ForgotPasswordModel model)
        {
            var result = new ResponseModel();
            var user = await _userManager.FindByNameAsync(model.UserName);
            var token = await _userManager.GeneratePasswordResetTokenAsync(user);

            if (!string.IsNullOrEmpty(token))
            {
                result.Success = true;
                result.Response = token;
            }

            return result;
        }

        public async Task<bool> ResetPsssword(ResetPasswordModel model)
        {
            var user = await _userManager.FindByNameAsync(model.UserName);
            var result = await _userManager.ResetPasswordAsync(user, model.Token, model.Password);
            if (result.Succeeded)
            {
                return true;
            }
            return false;
        }

        public async Task<PagedResponseModel> GetEmployees(int limit, int offset)
        {
            var response = new PagedResponseModel();

            var role = Role.Employee.ToString();
            response.Total = _context.UserInfotmation.Where(x => x.Role == role).Count();
            if (response.Total == 0)
            {
                response.Success = true;
                return response;
            }

            var employees = _context.UserInfotmation.Where(x => x.Role == role).OrderByDescending(x => x.CreateDate).Skip(offset).Take(limit).ToList();
            var items = employees.Select(x => new ProfileModel
            {
                UserId = x.UserId,
                FullName = x.FullName,
                Mobile = x.Mobile,
                Address = x.Address,
                Role = role,
                Email = x.Email,
                MediaId = x.MediaId
            }).ToList();

            response.Response = items;
            return response;
        }

        public int UserCount()
        {
            return _context.UserInfotmation.Where(x => x.Role != Role.SuperAdmin.ToString()).Count();
        }

        public int EmployeeCount()
        {
            return _context.UserInfotmation.Where(x => x.Role == Role.Employee.ToString()).Count();
        }

        public int CustomerCount()
        {
            return _context.UserInfotmation.Where(x => x.Role == Role.Customer.ToString()).Count();
        }
    }
}
