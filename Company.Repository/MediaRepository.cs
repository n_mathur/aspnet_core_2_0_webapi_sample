﻿using Company.Domain;
using Company.Domain.Enum;
using Company.Domain.Models;
using Company.Domain.Models.Response;
using Company.Repository.Entities;
using Company.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company.Repository
{
    public class MediaRepository : IMediaRepository
    {
        protected readonly CompanyContext _context;
         
        public MediaRepository(CompanyContext context)
        {
            _context = context;
        }

        public ResponseModel AddMedia(List<MediaManageModel> models, int createBy)
        {
            var date = DateTime.Now;
            var mediaList = new List<Media>();
            foreach (var model in models)
            {
                var _media = new Media
                {
                    Title = model.Title,
                    Description = model.Description,
                    Filename = model.Filename,
                    CreateBy = createBy,
                    CreateDate = date,
                    ModifyBy = createBy,
                    ModifyDate = date
                };

                _context.Add(_media);
                mediaList.Add(_media);
            }
            int count = _context.SaveChanges();

            var mediaResponse = mediaList.Select(x => new MediaModel
            {
                MediaId = x.MediaId,
                Title = x.Title,
                Description = x.Description,
                Filename = x.Filename,
                Path = Constants.MediaBaseUrl + x.Filename,
                CreateBy = x.CreateBy,
                CreateDate = x.CreateDate,
                ModifyBy = x.ModifyBy,
                ModifyDate = x.ModifyDate
            }).ToList();
            return new ResponseModel { Success = count > 0, Response = mediaResponse };
        }

        public ResponseModel UpdateMedia(MediaManageModel model, int modifyBy)
        {
            var _media = _context.Media.Where(x => x.MediaId == model.MediaId).FirstOrDefault();
            if (_media == null)
            {
                return new ResponseModel { Success = true };
            }

            var date = DateTime.Now;

            _media.Title = model.Title;
            _media.Description = model.Description;
            _media.Filename = model.Filename;
            _media.ModifyBy = modifyBy;
            _media.ModifyDate = date;

            _context.Update(_media);
            int count = _context.SaveChanges();

            return new ResponseModel { Success = count > 0 };
        }

        public ResponseModel DeleteMedia(int mediaId)
        {
            var Media = _context.Media.Where(x => x.MediaId == mediaId).FirstOrDefault();

            if (Media == null)
            {
                return new ResponseModel { Success = true };
            }

            _context.Remove(Media);
            int count = _context.SaveChanges();

            return new ResponseModel { Success = count > 0 };
        }

        public MediaModel GetMedia(int mediaId)
        {
            var _media = _context.Media.Where(x => x.MediaId == mediaId).FirstOrDefault();
            if (_media == null)
            {
                return new MediaModel();
            }
            return new MediaModel
            {
                MediaId = _media.MediaId,
                Title = _media.Title,
                Description = _media.Description,
                Filename = _media.Filename,
                Path = Constants.MediaBaseUrl + _media.Filename,
                CreateBy = _media.CreateBy,
                CreateDate = _media.CreateDate,
                ModifyBy = _media.ModifyBy,
                ModifyDate = _media.ModifyDate
            };
        }

        public List<MediaModel> GetMedia(List<int> mediaIds)
        {
            var medias = _context.Media.Where(x => mediaIds.Contains(x.MediaId)).ToList();
            if (medias == null)
            {
                return new List<MediaModel>();
            }
            return medias.Select(x => new MediaModel
            {
                MediaId = x.MediaId,
                Title = x.Title,
                Description = x.Description,
                Filename = x.Filename,
                Path = Constants.MediaBaseUrl + x.Filename,
                CreateBy = x.CreateBy,
                CreateDate = x.CreateDate,
                ModifyBy = x.ModifyBy,
                ModifyDate = x.ModifyDate
            }).ToList();
        }
    }
}
