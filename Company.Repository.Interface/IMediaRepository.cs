﻿using Company.Domain.Models;
using Company.Domain.Models.Response;
using Microsoft.AspNetCore.Authentication;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Company.Repository.Interface
{
    public interface IMediaRepository
    {
        ResponseModel AddMedia(List<MediaManageModel> models, int createBy);
        ResponseModel UpdateMedia(MediaManageModel model, int modifyBy);
        ResponseModel DeleteMedia(int mediaId);
        MediaModel GetMedia(int mediaId);
        List<MediaModel> GetMedia(List<int> mediaIds);
    }
}
