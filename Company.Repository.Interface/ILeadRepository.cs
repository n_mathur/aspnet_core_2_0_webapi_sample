﻿using Company.Domain.Models;
using Company.Domain.Models.Response;
using Microsoft.AspNetCore.Authentication;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Company.Repository.Interface
{
    public interface ILeadRepository
    {
        ResponseModel AddLead(AddLeadModel model);
        PagedResponseModel GetLeads(int limit, int offset);
    }
}
