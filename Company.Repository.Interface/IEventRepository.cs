﻿using Company.Domain.Models;
using Company.Domain.Models.Response;
using Microsoft.AspNetCore.Authentication;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Company.Repository.Interface
{
    public interface IEventRepository
    {
         ResponseModel AddEvent(AddEventModel model, int createBy);
         ResponseModel UpdateEvent(UpdateEventModel model, int modifyBy);
         ResponseModel DeleteEvent(int eventId);
         EventModel GetEvent(int eventId);
        PagedResponseModel GetEvents(int limit, int offset);
    }
}
