﻿using Company.Domain.Models;
using Company.Domain.Models.Response;
using Microsoft.AspNetCore.Authentication;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Company.Repository.Interface
{
    public interface IDepartmentRepository
    {
        ResponseModel AddDepartment(AddDepartmentModel model, int createBy);
        ResponseModel UpdateDepartment(UpdateDepartmentModel model, int modifyBy);
        ResponseModel DeleteDepartment(int productId);
        DepartmentModel GetDepartment(int productId);
        List<DepartmentModel> GetDepartments();
    }
}
