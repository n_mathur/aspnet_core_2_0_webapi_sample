﻿using Company.Domain.Models;
using Company.Domain.Models.Response;
using Microsoft.AspNetCore.Authentication;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Company.Repository.Interface
{
    public interface IProductRepository
    {
        ResponseModel AddProducts(List<AddBulkProductModel> models, int createBy);
        ResponseModel AddProduct(AddProductModel model, int createBy);
        ResponseModel UpdateProduct(UpdateProductModel model, int modifyBy);
        ResponseModel DeleteProduct(int bannerId);
        ProductModel GetProduct(int bannerId);
        List<ProductModel> GetProducts();

        ResponseModel AddProductMedia(int bannerId, List<int> mediaIds);

        List<ProductMediaModel> GetProductMedia(int bannerId);
        List<ProductMediaModel> GetProductMedia(List<int> bannerIds);
        ResponseModel DeleteProductMedia(int bannerMediaId);
    }
}
