﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Company.Domain.Validation
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    class DateAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var dateString = value as string;
            if (string.IsNullOrWhiteSpace(dateString))
            {
                return true; // Not our problem
            }
            try
            {
                DateTime.ParseExact(dateString, Constants.DateTimeFormat, CultureInfo.InvariantCulture);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}



