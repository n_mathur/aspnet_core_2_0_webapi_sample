﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Company.Domain.Enum
{
    public enum Role : int
    {
        [Description("SuperAdmin")]
        SuperAdmin = 1,
        [Description("Employee")]
        Employee = 2,
        [Description("Customer")]
        Customer = 3
    };
}
