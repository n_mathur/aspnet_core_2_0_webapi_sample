﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company.Domain
{
    public static class Constants
    {
        public const string MediaBaseUrl = "http://localhost:7400/media/";
        public const string DateTimeFormat = "dd-MM-yyyy HH:mm";
    }
}
