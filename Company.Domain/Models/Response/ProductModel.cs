﻿


using Company.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Company.Domain.Models
{
    public class ProductModel : BaseView
    {
        public int ProductId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public int Price { get; set; }
        public int Discount { get; set; }
        public bool Active { get; set; }
        public List<ProductMediaModel> Media { get; set; }
    }

    public class ProductMediaModel : MediaModel
    {
        public int ProductMediaId { get; set; }
        public int ProductId { get; set; }
    }
}
