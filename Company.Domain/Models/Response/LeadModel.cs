﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company.Domain.Models.Response
{
    public class LeadModel
    {
        public int LeadId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Comments { get; set; }
        public DateTime Date { get; set; }
    }
}
