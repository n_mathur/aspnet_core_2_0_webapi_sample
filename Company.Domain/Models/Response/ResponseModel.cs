﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company.Domain.Models
{
    public class ResponseModel
    {
        public object Response { get; set; }
        public bool Success { get; set; }
    }

    public class PagedResponseModel : ResponseModel
    {
        public int Total { get; set;}
    }
}
