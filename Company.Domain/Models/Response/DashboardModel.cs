﻿using Company.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Company.Domain.Models
{
    public class AdminDashboardCountsModel
    {
        public int UserCount { get; set; }
        public int TestCount { get; set; }
        public int QuestionCount { get; set; }
        public int EmployeeTestCount { get; set; }
    }

    public class EmployeeDashboardCountsModel
    {
        public int TestCount { get; set; }
    }
}