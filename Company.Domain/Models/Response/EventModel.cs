﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company.Domain.Models
{
    public class EventModel : BaseView
    {
        public int EventId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Active { get; set; }
    }
}
