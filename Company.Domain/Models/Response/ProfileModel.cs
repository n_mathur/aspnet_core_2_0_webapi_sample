﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Company.Domain.Models.Response
{
    public class ProfileModel
    {
        public int UserId { get; set; }
        public string FullName { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public int MediaId { get; set; }
        public string Avatar { get; set; }
    }
}
