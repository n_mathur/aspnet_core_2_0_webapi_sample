﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company.Domain.Models
{
    public class MediaModel : BaseView
    {
        public int MediaId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Filename { get; set; }
        public string Path { get; set; }
    }
}
