﻿


using Company.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Company.Domain.Models
{
    public class DepartmentModel : BaseView
    {
        public int DepartmentId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
     }
}
