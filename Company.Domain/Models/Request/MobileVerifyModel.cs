﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Company.Domain.Models
{
    public class MobileVerifyModel
    {
        public string UserName { get; set; }
        public string Token { get; set; }
    }
}
