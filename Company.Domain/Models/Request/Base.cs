﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company.Domain.Models
{
    public class Base
    {
        public int CreateBy {get; set;}
        public DateTime CreateDate { get; set; }
        public int ModifyBy { get; set; }
        public DateTime ModifyDate { get; set; }
    }

    public class BaseView : Base
    {
        public string CreateByName { get; set; }
        public string ModifyByName { get; set; }
    }
}
