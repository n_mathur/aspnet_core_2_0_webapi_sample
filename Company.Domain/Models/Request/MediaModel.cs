﻿using Company.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Company.Domain.Models
{
    public class MediaManageModel
    {
        public string Title { get; set; }

        public string Description { get; set; }
        
        public string Filename { get; set; }

        public int MediaId { get; set; }
    }
}
