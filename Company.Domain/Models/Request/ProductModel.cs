﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Company.Domain.Models
{
    public class AddBulkProductModel
    {
        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        public int Quantity { get; set; }
        public int Price { get; set; }
        public int Discount { get; set; }
    }

    public class AddProductModel : AddBulkProductModel
    {
        public string MediaIds { get; set; }
    }

    public class UpdateProductModel : AddProductModel
    {
        public int ProductId { get; set; }
        public bool Active { get; set; }
    }    

    public class AddProductMediaModel
    {
        public int ProductId { get; set; }
        public string MediaIds { get; set; }
    }
}
