﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Company.Domain.Models
{
    public class AddDepartmentModel
    {
        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

    }
    public class UpdateDepartmentModel : AddProductModel
    {
        public int DepartmentId { get; set; }        
    }   
}
