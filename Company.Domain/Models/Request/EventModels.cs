﻿using Company.Domain.Enum;
using Company.Domain.Validation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Company.Domain.Models
{
    public class AddEventModel
    {
        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }

        public string Description { get; set; }

        [Required]
        [Date(ErrorMessage = "Please enter valid date")]
        public string StartDate { get; set; }

        [Required]
        [Date(ErrorMessage = "Please enter valid date")]
        public string EndDate { get; set; }

        [JsonIgnore]
        public DateTime Start { get; set; }

        [JsonIgnore]
        public DateTime End { get; set; }
    }

    public class UpdateEventModel : AddEventModel
    {
        public int EventId { get; set; }

        public bool Active { get; set; }
    }
}
