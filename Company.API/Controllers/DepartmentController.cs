﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Company.Business.Interface;
using Company.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Company.API.Controllers
{
    [ApiVersion("1.0")]
    [EnableCors("AllowCors"),  Route("v{version:apiVersion}")]
    public class DepartmentController : Controller
    {
        private IDepartmentBusiness _departmentBusiness;
        private IUserBusiness _userBusiness;

        public DepartmentController(IDepartmentBusiness departmentBusiness, IUserBusiness userBusiness)
        {
            _departmentBusiness = departmentBusiness;
            _userBusiness = userBusiness;
        }
        
        [HttpPost]
        [Route("department")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "SuperAdmin")]
        public ActionResult AddDepartment([FromBody]AddDepartmentModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userName = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var userProfile = _userBusiness.GetProfile(userName);
            var result = _departmentBusiness.AddDepartment(model, userProfile.UserId);
            if (result.Success)
            {
                result.Response = "Success";
            }
            else
            {
                result.Response = "Failed";
            }

            return Json(result);
        }

        [HttpGet]
        [Route("department/{departmentId}")]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public ActionResult GetDepartment(int departmentId)
        {
            var result = _departmentBusiness.GetDepartment(departmentId);
            return Json(result);
        }

        [HttpGet]
        [Route("department")]
        public ActionResult GetDepartments()
        {
            var result = _departmentBusiness.GetDepartments();
            return Json(result);
        }

        [HttpPut]
        [Route("department")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "SuperAdmin")]
        public ActionResult UpdateDepartment([FromBody]UpdateDepartmentModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userName = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var userProfile = _userBusiness.GetProfile(userName);
            var result = _departmentBusiness.UpdateDepartment(model, userProfile.UserId);
            if (result.Success)
            {
                result.Response = "Success";
            }
            else
            {
                result.Response = "Failed";
            }

            return Json(result);
        }

        [HttpDelete]
        [Route("department/{departmentId}")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "SuperAdmin")]
        public ActionResult DeleteDepartment(int departmentId)
        {
            var result = _departmentBusiness.DeleteDepartment(departmentId);
            if (result.Success)
            {
                result.Response = "Success";
            }
            else
            {
                result.Response = "Failed";
            }

            return Json(result);
        }
    }
}