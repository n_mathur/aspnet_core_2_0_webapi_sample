﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Company.Domain.Models;
using Company.Business.Interface;
using Microsoft.AspNetCore.Mvc;
using Company.Repository.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Company.API.Helpers;
using Company.Domain.Models.Response;
using Microsoft.AspNetCore.Cors;

namespace Company.API.Controllers
{
    [EnableCors("AllowCors")]
    public class AccountController : Controller
    {
        private IUserBusiness _business;
        private IMediaBusiness _mediaBusiness;

        public AccountController(IUserBusiness business, IMediaBusiness mediaBusiness)
        {
            _business = business;
            _mediaBusiness = mediaBusiness;
        }
                
        // POST register
        [HttpPost]
        [Route("register")]
        public async Task<ActionResult> Register([FromBody]RegisterModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _business.RegisterUser(model);
            if (result.Success)
            {
                return Json(result);
            }
            else
            {
                return Json(new ResponseModel { Success = false, Response = "Failed" });
            }
        }
        
        // POST email/comfirm
        [HttpPost]
        [Route("verify")]
        public async Task<ActionResult> PhoneValidate([FromBody]MobileVerifyModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await _business.ConfirmMobile(model);

            if (result.Success)
            {
                return Json(result);
            }
            else
            {
                return Json(new ResponseModel { Success = false, Response = "Failed" });
            }
        }
        
        // GET profile
        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Route("profile")]
        public IActionResult GetProfile()
        {
            var userName = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var profile = _business.GetProfile(userName);
            if(profile.MediaId > 0)
            {
                var media = _mediaBusiness.GetMedia(profile.MediaId);
                profile.Avatar = media.Path;
            }
            else
            {
                profile.Avatar = MediaHelper.DefaultMediaPath();
            }
            return Json(profile);
        }

        // PUT profile
        [HttpPut]
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Route("profile")]
        public IActionResult UpdateProfile([FromBody]ProfileUpdateModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var userName = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var result = _business.UpdateProfile(model, userName);
            if (result)
            {
                return Json(new ResponseModel { Success = true, Response = "Success" });
            }
            else
            {
                return Json(new ResponseModel { Success = false, Response = "Failed" });
            }
        }

        // PUT profile
        [HttpPut]
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Route("password/change")]
        public async Task<ActionResult> ChangePassword([FromBody]ChangePasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var userName = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var result = await _business.ChangePsssword(model, userName);
            if (result)
            {
                return Json(new ResponseModel { Success = true, Response = "Success" });
            }
            else
            {
                return Json(new ResponseModel { Success = false, Response = "Failed" });
            }
        }

        // POST register
        [HttpPost]
        [Route("password/forgot")]
        public async Task<ActionResult> ForgotPassword([FromBody]ForgotPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _business.ForgotPassword(model);
            if (result.Success)
            {
                return Json(result);
            }
            else
            {
                return Json(new ResponseModel { Success = false, Response = "Failed" });
            }            
        }

        [HttpPost]
        [Route("password/reset")]
        public async Task<ActionResult> ResetPassword([FromBody]ResetPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _business.ResetPsssword(model);
            if (result)
            {
                return Json(new ResponseModel { Success = true, Response = "Success" });
            }
            else
            {
                return Json(new ResponseModel { Success = false, Response = "Failed" });
            }
        }
    }
}
