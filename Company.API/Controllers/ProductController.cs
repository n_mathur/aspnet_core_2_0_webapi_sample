﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Company.Business.Interface;
using Company.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Company.API.Controllers
{
    [ApiVersion("1.0")]
    [EnableCors("AllowCors"),  Route("v{version:apiVersion}")]
    public class ProductController : Controller
    {
        private IProductBusiness _productBusiness;
        private IUserBusiness _userBusiness;

        public ProductController(IProductBusiness productBusiness, IUserBusiness userBusiness)
        {
            _productBusiness = productBusiness;
            _userBusiness = userBusiness;
        }


        [HttpPost]
        [Route("product/bulk")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "SuperAdmin")]
        public ActionResult AddProducts([FromBody]List<AddBulkProductModel> model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userName = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var userProfile = _userBusiness.GetProfile(userName);
            var result = _productBusiness.AddProducts(model, userProfile.UserId);
            if (result.Success)
            {
                result.Response = "Success";
            }
            else
            {
                result.Response = "Failed";
            }

            return Json(result);
        }

        [HttpPost]
        [Route("product")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "SuperAdmin")]
        public ActionResult AddProduct([FromBody]AddProductModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userName = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var userProfile = _userBusiness.GetProfile(userName);
            var result = _productBusiness.AddProduct(model, userProfile.UserId);
            if (result.Success)
            {
                result.Response = "Success";
            }
            else
            {
                result.Response = "Failed";
            }

            return Json(result);
        }

        [HttpGet]
        [Route("product/{productId}")]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public ActionResult GetProduct(int productId)
        {
            var result = _productBusiness.GetProduct(productId);
            return Json(result);
        }

        [HttpGet]
        [Route("product")]
        public ActionResult GetProducts()
        {
            var result = _productBusiness.GetProducts();
            return Json(result);
        }

        [HttpPut]
        [Route("product")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "SuperAdmin")]
        public ActionResult UpdateProduct([FromBody]UpdateProductModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userName = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var userProfile = _userBusiness.GetProfile(userName);
            var result = _productBusiness.UpdateProduct(model, userProfile.UserId);
            if (result.Success)
            {
                result.Response = "Success";
            }
            else
            {
                result.Response = "Failed";
            }

            return Json(result);
        }

        [HttpDelete]
        [Route("product/{productId}")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "SuperAdmin")]
        public ActionResult DeleteProduct(int productId)
        {
            var result = _productBusiness.DeleteProduct(productId);
            if (result.Success)
            {
                result.Response = "Success";
            }
            else
            {
                result.Response = "Failed";
            }

            return Json(result);
        }

        [HttpPost]
        [Route("product/media")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "SuperAdmin")]
        public ActionResult AddProductMedia([FromBody]AddProductMediaModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var mediaIds = model.MediaIds.Split(",").Select(Int32.Parse).ToList();
            var result = _productBusiness.AddProductMedia(model.ProductId, mediaIds);
            if (result.Success)
            {
                result.Response = "Success";
            }
            else
            {
                result.Response = "Failed";
            }

            return Json(result);
        }

        [HttpDelete]
        [Route("product/media/{productMediaId}")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "SuperAdmin")]
        public ActionResult DeleteProductMedia(int productMediaId)
        {
            var result = _productBusiness.DeleteProductMedia(productMediaId);
            if (result.Success)
            {
                result.Response = "Success";
            }
            else
            {
                result.Response = "Failed";
            }

            return Json(result);
        }
    }
}