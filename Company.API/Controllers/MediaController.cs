﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Company.Business.Interface;
using Company.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Company.API.Controllers
{
    [ApiVersion("1.0")]
    [EnableCors("AllowCors"), Route("v{version:apiVersion}")]
    public class MediaController : Controller
    {
        private IMediaBusiness _mediaBusiness;
        private IUserBusiness _userBusiness;

        public MediaController(IMediaBusiness mediaBusiness, IUserBusiness userBusiness)
        {
            _mediaBusiness = mediaBusiness;
            _userBusiness = userBusiness;
        }

        [HttpPost]
        [Route("media")]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> AddMedia(List<IFormFile> files)
        {
            if (files == null || files.Count == 0)
                return BadRequest("files not selected");

            var userName = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var userProfile = _userBusiness.GetProfile(userName);
            var mediaModels = new List<MediaManageModel>();
            foreach (var file in files)
            {
                var title = file.FileName;
                var ext = Path.GetExtension(title);
                var filename = DateTime.Now.Ticks + ext;
                var path = Path.Combine(
                        Directory.GetCurrentDirectory(), "wwwroot/media",
                        filename);

                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }

                mediaModels.Add(new MediaManageModel
                {
                    Title = title,
                    Description = title,
                    Filename = filename
                });
            }

            var result = _mediaBusiness.AddMedia(mediaModels, userProfile.UserId);

            if (!result.Success)            
            {
                result.Response = "Failed";
            }

            return Json(result);
        }

        [HttpGet]
        [Route("media/{mediaId}")]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public ActionResult GetMedia(int mediaId)
        {
            var result = _mediaBusiness.GetMedia(mediaId);
            return Json(result);
        }

        [HttpPut]
        [Route("media")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "SuperAdmin")]
        public ActionResult UpdateMedia([FromBody]MediaManageModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userName = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var userProfile = _userBusiness.GetProfile(userName);
            var result = _mediaBusiness.UpdateMedia(model, userProfile.UserId);
            if (result.Success)
            {
                result.Response = "Success";
            }
            else
            {
                result.Response = "Failed";
            }

            return Json(result);
        }

        [HttpDelete]
        [Route("media/{mediaId}")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "SuperAdmin")]
        public ActionResult DeleteMedia(int mediaId)
        {
            var result = _mediaBusiness.DeleteMedia(mediaId);
            if (result.Success)
            {
                result.Response = "Success";
            }
            else
            {
                result.Response = "Failed";
            }

            return Json(result);
        }
    }
}