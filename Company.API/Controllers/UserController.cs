﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Company.API.Helpers;
using Company.Business.Interface;
using Company.Domain.Models;
using Company.Domain.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Company.API.Controllers
{
    [ApiVersion("1.0")]
    [EnableCors("AllowCors"), Route("v{version:apiVersion}")]
    public class UserController : Controller
    {
        private IUserBusiness _business;
        private IMediaBusiness _mediaBusiness;

        public UserController(IUserBusiness business, IMediaBusiness mediaBusiness)
        {
            _business = business;
            _mediaBusiness = mediaBusiness;
        }

        [HttpGet]
        [Route("employees/{limit}/{offset}")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "SuperAdmin")]
        public async Task<ActionResult> GetEmployees(int limit, int offset)
        {
            var result = await _business.GetEmployees(limit, offset);
            
            var defaultAvatar = MediaHelper.DefaultMediaPath();

            var medias = new List<MediaModel>();

            var employees = (List<ProfileModel>)result.Response;

            var mediaIds = employees.Where(x => x.MediaId != 0).Select(x => x.MediaId).ToList();
            if (mediaIds != null && mediaIds.Count > 0)
            {
                medias = _mediaBusiness.GetMedia(mediaIds);
            }

            employees.Select(x =>
            {
                if (x.MediaId > 0)
                {
                    var media = medias.Where(m => m.MediaId == x.MediaId).FirstOrDefault();
                    x.Avatar = media.Path;
                }
                else
                {
                    x.Avatar = defaultAvatar;
                }
                return x;
            }).ToList();

            result.Response = employees;
            return Json(result);
        }

        [HttpGet]
        [Route("user/{userId}")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "SuperAdmin")]
        public ActionResult GetUserProfile(int userId)
        {
            var profile = _business.GetProfileByUser(userId);

            if (profile.MediaId > 0)
            {
                var media = _mediaBusiness.GetMedia(profile.MediaId);
                profile.Avatar = media.Path;
            }
            else
            {
                profile.Avatar = MediaHelper.DefaultMediaPath();
            }
            return Json(profile);
        }

        [HttpPut]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "SuperAdmin")]
        [Route("user")]
        public IActionResult UpdateUserProfile([FromBody]UserProfileUpdateModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = _business.UpdateUserProfile(model);
            if (result)
            {
                return Json(new ResponseModel { Success = true, Response = "Success" });
            }
            else
            {
                return Json(new ResponseModel { Success = false, Response = "Failed" });
            }
        }
    }
}