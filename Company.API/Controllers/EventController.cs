﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Company.Business.Interface;
using Company.Domain;
using Company.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Company.API.Controllers
{
    [ApiVersion("1.0")]
    [EnableCors("AllowCors"),  Route("v{version:apiVersion}")]
    public class EventController : Controller
    {
        private IEventBusiness _eventBusiness;
        private IUserBusiness _userBusiness;

        public EventController(IEventBusiness eventBusiness, IUserBusiness userBusiness)
        {
            _eventBusiness = eventBusiness;
            _userBusiness = userBusiness;
        }

        [HttpPost]
        [Route("event")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "SuperAdmin")]
        public ActionResult AddEvent([FromBody]AddEventModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            model.Start = DateTime.ParseExact(model.StartDate, Constants.DateTimeFormat, CultureInfo.InvariantCulture);
            model.End = DateTime.ParseExact(model.EndDate, Constants.DateTimeFormat, CultureInfo.InvariantCulture);

            var userName = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var userProfile = _userBusiness.GetProfile(userName);
            var result = _eventBusiness.AddEvent(model, userProfile.UserId);
            if (result.Success)
            {
                result.Response = "Success";
            }
            else
            {
                result.Response = "Failed";
            }

            return Json(result);
        }

        [HttpGet]
        [Route("event/{eventId}")]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public ActionResult GetEvent(int eventId)
        {
            var result = _eventBusiness.GetEvent(eventId);
            return Json(result);
        }

        [HttpGet]
        [Route("event/{limit}/{offset}")]
        public ActionResult GetEvents(int limit, int offset)
        {
            var result = _eventBusiness.GetEvents(limit, offset);
            return Json(result);
        }

        [HttpPut]
        [Route("event")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "SuperAdmin")]
        public ActionResult UpdateEvent([FromBody]UpdateEventModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            model.Start = DateTime.ParseExact(model.StartDate, Constants.DateTimeFormat, CultureInfo.InvariantCulture);
            model.End = DateTime.ParseExact(model.EndDate, Constants.DateTimeFormat, CultureInfo.InvariantCulture);

            var userName = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var userProfile = _userBusiness.GetProfile(userName);
            var result = _eventBusiness.UpdateEvent(model, userProfile.UserId);
            if (result.Success)
            {
                result.Response = "Success";
            }
            else
            {
                result.Response = "Failed";
            }

            return Json(result);
        }

        [HttpDelete]
        [Route("event/{eventId}")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "SuperAdmin")]
        public ActionResult DeleteEvent(int eventId)
        {
            var result = _eventBusiness.DeleteEvent(eventId);
            if (result.Success)
            {
                result.Response = "Success";
            }
            else
            {
                result.Response = "Failed";
            }

            return Json(result);
        }        
    }
}