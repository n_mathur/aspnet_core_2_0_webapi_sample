﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Company.Domain.Models;
using Company.Business.Interface;
using Microsoft.AspNetCore.Mvc;
using Company.Repository.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Company.API.Helpers;
using Company.Domain.Models.Response;
using Microsoft.AspNetCore.Cors;

namespace Company.API.Controllers
{
    [EnableCors("AllowCors")]
    public class AuthController : Controller
    {
        private IUserBusiness _business;

        public AuthController(IUserBusiness business)
        {
            _business = business;
        }

        [HttpPost("token")]
        public async Task<IActionResult> Token([FromBody]LoginModel model)
        {
            if (string.IsNullOrEmpty(model.Username) && string.IsNullOrEmpty(model.Password))
            {
                return Json(new ResponseModel { Success = false, Response = "Invalid username or password." });
            }

            var response = await _business.Validate(model);
            if (!response.Success)
            {
                return Json(new ResponseModel { Success = false, Response = "Invalid username or password." });
            }

            var token = new JwtTokenBuilder()
                                .AddSecurityKey(JwtSecurityKey.Create("namrata-9351-c1f612ae938f9351-c1f612ae938f-mathur"))
                                .AddSubject(model.Username)
                                .AddIssuer("Company.API")
                                .AddAudience("Company.API")
                                .AddClaim("UserName", model.Username)
                                .AddClaim(ClaimTypes.Role, (string)response.Response)
                                .AddExpiry(1)
                                .Build();

            return Json(new ResponseModel { Success = true, Response = token.Value });
        }
    }
}
