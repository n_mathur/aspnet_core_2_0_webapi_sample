﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Company.Business.Interface;
using Company.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Company.API.Controllers
{
    [ApiVersion("1.0")]
    [EnableCors("AllowCors"), Route("v{version:apiVersion}")]
    public class LeadController : Controller
    {
        private ILeadBusiness _business;

        public LeadController(ILeadBusiness business)
        {
            _business = business;
        }

        [HttpPost]
        [Route("Lead")]
        public ActionResult AddLead([FromBody]AddLeadModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = _business.AddLead(model);
            if (result.Success)
            {
                result.Response = "Success";
            }
            else
            {
                result.Response = "Failed";
            }

            return Json(result);
        }

        [HttpGet]
        [Route("Lead/{limit}/{offset}")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "SuperAdmin")]
        public ActionResult GetLeads(int limit, int offset)
        {
            var result = _business.GetLeads(limit, offset);
            return Json(result);
        }        
    }
}