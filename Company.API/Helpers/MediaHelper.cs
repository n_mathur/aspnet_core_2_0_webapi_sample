﻿using Company.Business.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Company.API.Helpers
{
    public static class MediaHelper
    { 
        public static string DefaultMediaPath()
        {
            return "user-avatar.jpg";
        }
    }
}
