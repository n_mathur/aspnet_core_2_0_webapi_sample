﻿using Company.API.Helpers;
using Company.Business;
using Company.Business.Interface;
using Company.Repository;
using Company.Repository.Entities;
using Company.Repository.Interface;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Company.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<CompanyContext>(options =>
                options.UseMySql(Configuration.GetConnectionString("CompanyConnection")));

            services.AddIdentity<ApplicationUser, ApplicationRole>()
               .AddEntityFrameworkStores<CompanyContext>()
               .AddDefaultTokenProviders();

            // Add application services.

            //using Dependency Injection
            services.AddScoped<IUserBusiness, UserBusiness>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IDepartmentBusiness, DepartmentBusiness>();
            services.AddScoped<ILeadBusiness, LeadBusiness>();
            services.AddScoped<IEventBusiness, EventBusiness>();
            services.AddScoped<IMediaBusiness, MediaBusiness>();          
            services.AddScoped<IProductBusiness, ProductBusiness>();

            services.AddScoped<IDepartmentRepository, DepartmentRepository>();
            services.AddScoped<ILeadRepository, LeadRepository>();
            services.AddScoped<IEventRepository, EventRepository>();
            services.AddScoped<IMediaRepository, MediaRepository>();         
            services.AddScoped<IProductRepository, ProductRepository>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = "Company.API",
                    ValidAudience = "Company.API",
                    IssuerSigningKey = JwtSecurityKey.Create("namrata-9351-c1f612ae938f9351-c1f612ae938f-mathur")
                };
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "AWP API",
                    Description = "A ...."
                });

                c.AddSecurityDefinition("Bearer", new ApiKeyScheme { In = "header", Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"", Name = "Authorization", Type = "apiKey" });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> {
                { "Bearer", Enumerable.Empty<string>() } });

                // Set the comments path for the Swagger JSON and UI.
                var basePath = AppContext.BaseDirectory;
                var xmlPath = Path.Combine(basePath, "Company.API.xml");
                c.IncludeXmlComments(xmlPath);

            });

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 5;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
            });

            services.Configure<IISOptions>(options =>
            {
                options.ForwardClientCertificate = false;
                options.AutomaticAuthentication = true;
            });

            services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = new ApiVersion(1, 0);
            });

            services.AddMvc();
            services.AddCors(options =>
            options.AddPolicy("AllowCors",
            builder =>
            {
                builder.WithOrigins("http://localhost:4200")
                       .WithMethods("GET", "PUT", "POST", "DELETE")
                       .WithHeaders("Accept", "Content-type", "Origin", "X-Custom-Header", "Authorization");
            }));
        }



        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Company API V1");
                c.DefaultModelRendering(ModelRendering.Example);
                c.DefaultModelsExpandDepth(-1);
            });

            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<CompanyContext>();

                UserManager<ApplicationUser> userManager = serviceScope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                ILogger<CompanyInitializer> logger = serviceScope.ServiceProvider.GetRequiredService<ILogger<CompanyInitializer>>();
                RoleManager<ApplicationRole> roleManager = serviceScope.ServiceProvider.GetRequiredService<RoleManager<ApplicationRole>>();

                CompanyInitializer.Initialize(context, userManager, roleManager, logger);
            }

            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseMvcWithDefaultRoute();

            app.UseMvc();
            app.UseCors("AllowCors");
        }
    }
}
