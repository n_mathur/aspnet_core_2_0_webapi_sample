﻿using Company.Domain.Models;
using Company.Business.Interface;
using Company.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Company.Domain.Models.Response;
using System.Linq;

namespace Company.Business
{
    public class MediaBusiness : IMediaBusiness
    {
        readonly IMediaRepository _mediaRepository;
        public MediaBusiness(IMediaRepository MediaRepository)
        {
            _mediaRepository = MediaRepository;
        }
       
        public ResponseModel AddMedia(List<MediaManageModel> models, int createBy)
        {
            return _mediaRepository.AddMedia(models, createBy);
        }

        public ResponseModel UpdateMedia(MediaManageModel model, int modifyBy)
        {
            return _mediaRepository.UpdateMedia(model, modifyBy);
        }

        public ResponseModel DeleteMedia(int mediaId)
        {
            return _mediaRepository.DeleteMedia(mediaId);
        }

        public MediaModel GetMedia(int mediaId)
        {
            var media = _mediaRepository.GetMedia(mediaId);
            return media;
        }
        
        public List<MediaModel> GetMedia(List<int> mediaIds)
        {
            var result = _mediaRepository.GetMedia(mediaIds);
            return result;
        }
    }
}
