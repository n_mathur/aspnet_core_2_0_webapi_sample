﻿using Company.Domain.Models;
using Company.Business.Interface;
using Company.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Company.Domain.Models.Response;
using System.Linq;

namespace Company.Business
{
    public class LeadBusiness : ILeadBusiness
    {
        readonly ILeadRepository _LeadRepository;
        public LeadBusiness(ILeadRepository LeadRepository)
        {
            _LeadRepository = LeadRepository;
        }

        public ResponseModel AddLead(AddLeadModel model)
        {
            return _LeadRepository.AddLead(model);
        }
        
        public PagedResponseModel GetLeads(int limit, int offset)
        {
            return _LeadRepository.GetLeads(limit, offset);
        }
    }
}
