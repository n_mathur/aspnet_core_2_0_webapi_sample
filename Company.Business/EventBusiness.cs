﻿using Company.Domain.Models;
using Company.Business.Interface;
using Company.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Company.Domain.Models.Response;

namespace Company.Business
{
    public class EventBusiness : IEventBusiness
    {
        readonly IEventRepository _eventRepository;
        public EventBusiness(IEventRepository EventRepository)
        {
            _eventRepository = EventRepository;
        }

        public ResponseModel AddEvent(AddEventModel model, int createBy)
        {
            return _eventRepository.AddEvent(model, createBy);
        }

        public ResponseModel UpdateEvent(UpdateEventModel model, int modifyBy)
        {
            return _eventRepository.UpdateEvent(model, modifyBy);
        }

        public ResponseModel DeleteEvent(int eventId)
        {
            return _eventRepository.DeleteEvent(eventId);
        }

        public EventModel GetEvent(int eventId)
        {
            return _eventRepository.GetEvent(eventId);
        }

        public PagedResponseModel GetEvents(int limit, int offset)
        {
            return _eventRepository.GetEvents(limit, offset);
        }
    }
}
