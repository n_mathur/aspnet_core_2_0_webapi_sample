﻿using Company.Domain.Models;
using Company.Business.Interface;
using Company.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Company.Domain.Models.Response;
using System.Linq;

namespace Company.Business
{
    public class ProductBusiness : IProductBusiness
    {
        readonly IProductRepository _productRepository;
        readonly IMediaBusiness _mediaBusiness;
        public ProductBusiness(IProductRepository productRepository, IMediaBusiness mediaBusiness)
        {
            _productRepository = productRepository;
            _mediaBusiness = mediaBusiness;
        }

        public ResponseModel AddProducts(List<AddBulkProductModel> models, int createBy)
        {
            return _productRepository.AddProducts(models, createBy);
        }

        public ResponseModel AddProduct(AddProductModel model, int createBy)
        {
            return _productRepository.AddProduct(model, createBy);
        }

        public ResponseModel UpdateProduct(UpdateProductModel model, int modifyBy)
        {
            return _productRepository.UpdateProduct(model, modifyBy);
        }

        public ResponseModel DeleteProduct(int productId)
        {
            return _productRepository.DeleteProduct(productId);
        }

        public ProductModel GetProduct(int productId)
        {
            var result = _productRepository.GetProduct(productId);
            var productMedia = _productRepository.GetProductMedia(productId);
            var mediaIds = productMedia.Select(x => x.MediaId).ToList();
            var mediaModels = _mediaBusiness.GetMedia(mediaIds);

            productMedia.Select(x =>
            {
                var media = mediaModels.Where(m => m.MediaId == x.MediaId).FirstOrDefault();
                x.Title = media.Title;
                x.Description = media.Description;
                x.Filename = media.Filename;
                x.Path = media.Path;
                return x;
            }).ToList();
            result.Media = productMedia;

            return result;
        }

        public List<ProductModel> GetProducts()
        {
            var result = _productRepository.GetProducts();

            var productMedia = _productRepository.GetProductMedia(result.Select(x => x.ProductId).ToList());
            var mediaIds = productMedia.Select(x => x.MediaId).ToList();
            var mediaModels = _mediaBusiness.GetMedia(mediaIds);

            productMedia.Select(x =>
            {
                var media = mediaModels.Where(m => m.MediaId == x.MediaId).FirstOrDefault();
                x.Title = media.Title;
                x.Description = media.Description;
                x.Filename = media.Filename;
                x.Path = media.Path;
                return x;
            }).ToList();

            result.Select(x => { x.Media = productMedia.Where(m => m.ProductId == x.ProductId).ToList(); return x; }).ToList();
            return result;
        }

        public ResponseModel AddProductMedia(int productId, List<int> mediaIds)
        {
            return _productRepository.AddProductMedia(productId, mediaIds);
        }

        public List<ProductMediaModel> GetProductMedia(int productId)
        {
            return _productRepository.GetProductMedia(productId);
        }

        public List<ProductMediaModel> GetProductMedia(List<int> productIds)
        {
            return _productRepository.GetProductMedia(productIds);
        }
        public ResponseModel DeleteProductMedia(int productMediaId)
        {
            return _productRepository.DeleteProductMedia(productMediaId);
        }
    }
}
