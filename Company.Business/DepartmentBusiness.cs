﻿using Company.Domain.Models;
using Company.Business.Interface;
using Company.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Company.Domain.Models.Response;
using System.Linq;

namespace Company.Business
{
    public class DepartmentBusiness : IDepartmentBusiness
    {
        readonly IDepartmentRepository _departmentRepository;
        public DepartmentBusiness(IDepartmentRepository departmentRepository)
        {
            _departmentRepository = departmentRepository;
        }        

        public ResponseModel AddDepartment(AddDepartmentModel model, int createBy)
        {
            return _departmentRepository.AddDepartment(model, createBy);
        }

        public ResponseModel UpdateDepartment(UpdateDepartmentModel model, int modifyBy)
        {
            return _departmentRepository.UpdateDepartment(model, modifyBy);
        }

        public ResponseModel DeleteDepartment(int departmentId)
        {
            return _departmentRepository.DeleteDepartment(departmentId);
        }

        public DepartmentModel GetDepartment(int departmentId)
        {
            var result = _departmentRepository.GetDepartment(departmentId);    
            return result;
        }

        public List<DepartmentModel> GetDepartments()
        {
            var result = _departmentRepository.GetDepartments();
            return result;
        }       
    }
}
