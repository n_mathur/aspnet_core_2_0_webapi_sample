﻿using Company.Domain.Models;
using Company.Business.Interface;
using Company.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Company.Domain.Models.Response;

namespace Company.Business
{
    public class UserBusiness : IUserBusiness
    {
        readonly IUserRepository _userRepository;
        public UserBusiness(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public async Task<ResponseModel> RegisterUser(RegisterModel model)
        {
            return await _userRepository.RegisterUser(model);
        }

        public async Task<ResponseModel> ConfirmMobile(MobileVerifyModel model)
        {
            return await _userRepository.ConfirmMobile(model);
        }

        public async Task<ResponseModel> Validate(LoginModel model)
        {
            return await _userRepository.Validate(model);
        }

        public ProfileModel GetProfile(string userName)
        {
            var profile = _userRepository.GetProfile(userName);
            return profile;
        }

        public ProfileModel GetProfileByUser(int userId)
        {
            var profile = _userRepository.GetProfileByUser(userId);
            return profile;
        }

        public bool UpdateProfile(ProfileUpdateModel model, string userName)
        {
            return _userRepository.UpdateProfile(model, userName);
        }

        public bool UpdateUserProfile(UserProfileUpdateModel model)
        {
            return _userRepository.UpdateUserProfile(model);
        }

        public async Task<bool> ResetPsssword(ResetPasswordModel model)
        {
            return await _userRepository.ResetPsssword(model);
        }
        public async Task<bool> ChangePsssword(ChangePasswordModel model, string userName)
        {
            return await _userRepository.ChangePsssword(model, userName);
        }
        public async Task<ResponseModel> ForgotPassword(ForgotPasswordModel model)
        {
            return await _userRepository.ForgotPassword(model);
        }

        public async Task<PagedResponseModel> GetEmployees(int limit, int offset)
        {
            var employees = await _userRepository.GetEmployees(limit, offset);
            return employees;
        }
    }
}
