﻿using Company.Domain.Models;
using Company.Domain.Models.Response;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Company.Business.Interface
{
    public interface IProductBusiness
    {
        ResponseModel AddProducts(List<AddBulkProductModel> models, int createBy);
        ResponseModel AddProduct(AddProductModel model, int createBy);
        ResponseModel UpdateProduct(UpdateProductModel model, int modifyBy);
        ResponseModel DeleteProduct(int productId);
        ProductModel GetProduct(int productId);
        List<ProductModel> GetProducts();

        ResponseModel AddProductMedia(int productId, List<int> mediaIds);

        List<ProductMediaModel> GetProductMedia(int productId);
        List<ProductMediaModel> GetProductMedia(List<int> productIds);
        ResponseModel DeleteProductMedia(int productMediaId);
    }
}
