﻿using Company.Domain.Models;
using Company.Domain.Models.Response;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Company.Business.Interface
{
    public interface IUserBusiness
    {
        Task<ResponseModel> RegisterUser(RegisterModel model);
        Task<ResponseModel> ConfirmMobile(MobileVerifyModel model);
        Task<ResponseModel> Validate(LoginModel model);
        ProfileModel GetProfile(string userName);
        ProfileModel GetProfileByUser(int userId);

        bool UpdateProfile(ProfileUpdateModel model, string userName);
        bool UpdateUserProfile(UserProfileUpdateModel model);

        Task<bool> ResetPsssword(ResetPasswordModel model);
        Task<bool> ChangePsssword(ChangePasswordModel model, string userName);
        Task<ResponseModel> ForgotPassword(ForgotPasswordModel model);
        Task<PagedResponseModel> GetEmployees(int limit, int offset);
    }
}
