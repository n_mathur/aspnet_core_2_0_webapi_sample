﻿using Company.Domain.Models;
using Company.Domain.Models.Response;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Company.Business.Interface
{
    public interface IDepartmentBusiness
    {
        ResponseModel AddDepartment(AddDepartmentModel model, int createBy);
        ResponseModel UpdateDepartment(UpdateDepartmentModel model, int modifyBy);
        ResponseModel DeleteDepartment(int productId);
        DepartmentModel GetDepartment(int productId);
        List<DepartmentModel> GetDepartments();
    }
}
