﻿using Company.Domain.Models;
using Company.Domain.Models.Response;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Company.Business.Interface
{
    public interface ILeadBusiness
    {
        ResponseModel AddLead(AddLeadModel model);
        PagedResponseModel GetLeads(int limit, int offset);
    }
}
